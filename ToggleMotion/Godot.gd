extends Sprite2D

var speed = 400
var angular_speed = PI

# setup the node (~Start in Unity)
func _ready():
	var timer = get_node("./VisibilityTimer")
	timer.connect("timeout", self._on_Timmer_timeout)
	pass 

func _process(delta):
	rotation += angular_speed * delta
	var velocity = Vector2.UP.rotated(rotation) * speed
	position += velocity * delta


func _on_button_pressed():
	set_process(not is_processing())
	pass # Replace with function body.

func _on_Timmer_timeout():
	visible = not visible
	pass
