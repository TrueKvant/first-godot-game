extends Sprite2D

var angular_speed = PI
var linear_speed = 400.0

func _process(delta):
	# managing rotation
	# '-> ternary operators are same as in python
	var direction = -1 if Input.is_action_pressed("ui_left") else (1 if Input.is_action_pressed("ui_right") else 0)
	rotation += direction * angular_speed * delta
	
	# managing velocity
	# '-> ui stuff are placed in `Project > Input Map` and there we go
	var velocity = Vector2.UP if Input.is_action_pressed("ui_up") else Vector2.ZERO
	velocity = velocity.rotated(rotation)
	position += velocity * linear_speed * delta
	
