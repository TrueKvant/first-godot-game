extends Area2D

signal heath_changed(new_health)

var health = 10


func take_damage(amount):
	health -= amount
	if health < 0:
		health = 0
	# play animation
	get_node("AnimationPlayer").play("take_damage")
	emit_signal("heath_changed", health)

func _on_area_entered(area):
	take_damage(2)
